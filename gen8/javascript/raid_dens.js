// Den number lines up with serebii dens

export const dens = [
    {
        common: {
            hash: '173f0456dc5dfc52',
            den: 16,
        },
        rare: {
            hash: 'ba83e1671012ebcd',
            den: 52,
        },
        location: 12,
        x: 185,
        y: 977,
    },
    {
        common: {
            hash: '17458556dc634333',
            den: 37,
        },
        rare: {
            hash: 'ba8745671015cb90',
            den: 64,
        },
        location: 12,
        x: 125,
        y: 1005,
    },
    {
        common: {
            hash: '17458b56dc634d65',
            den: 31,
        },
        rare: {
            hash: '450421d99cf882c1',
            den: 90,
        },
        location: 12,
        x: 114,
        y: 936,
    },
    {
        common: {
            hash: '17428156dc610690',
            den: 3,
        },
        rare: {
            hash: 'ba805467100fc65f',
            den: 51,
        },
        location: 12,
        x: 311,
        y: 998,
    },
    {
        common: {
            hash: '17428856dc611275',
            den: 4,
        },
        rare: {
            hash: 'ba805767100fcb78',
            den: 46,
        },
        location: 12,
        x: 233,
        y: 948,
    },
    {
        common: {
            hash: '17458956dc6349ff',
            den: 33,
        },
        rare: {
            hash: 'ba8747671015cef6',
            den: 62,
        },
        location: 12,
        x: 337,
        y: 996,
    },
    {
        common: {
            hash: '17459356dc635afd',
            den: 39,
        },
        rare: {
            hash: 'ba8746671015cd43',
            den: 65,
        },
        location: 12,
        x: 209,
        y: 976,
    },
    {
        common: {
            hash: '17428356dc6109f6',
            den: 1,
        },
        rare: {
            hash: 'ba805967100fcede',
            den: 48,
        },
        location: 12,
        x: 136,
        y: 906,
    },
    {
        common: {
            hash: '17458b56dc634d65',
            den: 31,
        },
        rare: {
            hash: '450421d99cf882c1',
            den: 90,
        },
        location: 12,
        x: 252,
        y: 905,
    },
    {
        common: {
            hash: '17491656dc666f6d',
            den: 26,
        },
        rare: {
            hash: 'ba83da671012dfe8',
            den: 59,
        },
        location: 2,
        x: 30,
        y: 927,
    },
    {
        common: {
            hash: '17490856dc6657a3',
            den: 28,
        },
        rare: {
            hash: '4500a2d99cf5751d',
            den: 79,
        },
        location: 2,
        x: 12,
        y: 851,
    },
    {
        common: {
            hash: '17491656dc666f6d',
            den: 26,
        },
        rare: {
            hash: 'ba83db671012e19b',
            den: 58,
        },
        location: 2,
        x: 12,
        y: 861,
    },
    {
        common: {
            hash: '17428856dc611275',
            den: 4,
        },
        rare: {
            hash: '45041fd99cf87f5b',
            den: 92,
        },
        location: 2,
        x: 20,
        y: 913,
    },
    {
        common: {
            hash: '17491656dc666f6d',
            den: 26,
        },
        rare: {
            hash: 'ba83da671012dfe8',
            den: 59,
        },
        location: 2,
        x: 40,
        y: 878,
    },
    {
        common: {
            hash: '17428256dc610843',
            den: 2,
        },
        rare: {
            hash: 'ba805367100fc4ac',
            den: 50,
        },
        location: 15,
        x: 52,
        y: 776,
    },
    {
        common: {
            hash: '17428656dc610f0f',
            den: 6,
        },
        rare: {
            hash: 'ba805867100fcd2b',
            den: 47,
        },
        location: 15,
        x: 68,
        y: 652,
    },
    {
        common: {
            hash: '0000000000000000',
            den: 0,
        },
        rare: {
            hash: '0000000000000000',
            den: 0,
        },
        location: 15,
        x: 50,
        y: 700,
    },
    {
        common: {
            hash: '17428556dc610d5c',
            den: 7,
        },
        rare: {
            hash: 'ba805d67100fd5aa',
            den: 44,
        },
        location: 4,
        x: 217,
        y: 913,
    },
    {
        common: {
            hash: '17459356dc635afd',
            den: 39,
        },
        rare: {
            hash: 'ba8746671015cd43',
            den: 65,
        },
        location: 4,
        x: 158,
        y: 705,
    },
    {
        common: {
            hash: '17458b56dc634d65',
            den: 31,
        },
        rare: {
            hash: 'ba83d8671012dc82',
            den: 61,
        },
        location: 4,
        x: 220,
        y: 759,
    },
    {
        common: {
            hash: '17458b56dc634d65',
            den: 31,
        },
        rare: {
            hash: 'ba83d8671012dc82',
            den: 61,
        },
        location: 4,
        x: 248,
        y: 852,
    },
    {
        common: {
            hash: '17428b56dc61178e',
            den: 9,
        },
        rare: {
            hash: 'ba8a4e67101810b2',
            den: 75,
        },
        location: 16,
        x: 129,
        y: 818,
    },
    {
        common: {
            hash: '17428b56dc61178e',
            den: 9,
        },
        rare: {
            hash: 'ba8a4e67101810b2',
            den: 75,
        },
        location: 16,
        x: 131,
        y: 735,
    },
    {
        common: {
            hash: '17501a56dc6c94e7',
            den: 44,
        },
        rare: {
            hash: 'ba805d67100fd5aa',
            den: 42,
        },
        location: 16,
        x: 105,
        y: 907,
    },
    {
        common: {
            hash: '17428556dc610d5c',
            den: 7,
        },
        rare: {
            hash: 'ba805d67100fd5aa',
            den: 44,
        },
        location: 16,
        x: 50,
        y: 909,
    },
    {
        common: {
            hash: '17428b56dc61178e',
            den: 9,
        },
        rare: {
            hash: '450420d99cf8810e',
            den: 91,
        },
        location: 16,
        x: 98,
        y: 750,
    },
    {
        common: {
            hash: '17458756dc634699',
            den: 35,
        },
        rare: {
            hash: 'ba8748671015d0a9',
            den: 63,
        },
        location: 16,
        x: 107,
        y: 652,
    },
    {
        common: {
            hash: '17459256dc63594a',
            den: 38,
        },
        rare: {
            hash: 'ba8745671015cb90',
            den: 64,
        },
        location: 0,
        x: 186,
        y: 816,
    },
    {
        common: {
            hash: '17428c56dc611941',
            den: 8,
        },
        rare: {
            hash: '450420d99cf8810e',
            den: 91,
        },
        location: 13,
        x: 310,
        y: 824,
    },
    {
        common: {
            hash: '17501b56dc6c969a',
            den: 41,
        },
        rare: {
            hash: 'ba8a496710180833',
            den: 76,
        },
        location: 13,
        x: 359,
        y: 980,
    },
    {
        common: {
            hash: '17501b56dc6c969a',
            den: 41,
        },
        rare: {
            hash: 'ba8a496710180833',
            den: 76,
        },
        location: 13,
        x: 393,
        y: 962,
    },
    {
        common: {
            hash: '17428756dc6110c2',
            den: 5,
        },
        rare: {
            hash: 'ba805767100fcb78',
            den: 46,
        },
        location: 13,
        x: 328,
        y: 761,
    },
    {
        common: {
            hash: '17428356dc6109f6',
            den: 1,
        },
        rare: {
            hash: 'ba805c67100fd3f7',
            den: 43,
        },
        location: 13,
        x: 352,
        y: 765,
    },
    {
        common: {
            hash: '173f0356dc5dfa9f',
            den: 15,
        },
        rare: {
            hash: 'ba805467100fc65f',
            den: 51,
        },
        location: 7,
        x: 443,
        y: 895,
    },
    {
        common: {
            hash: '173f0056dc5df586',
            den: 12,
        },
        rare: {
            hash: 'ba805e67100fd75d',
            den: 45,
        },
        location: 7,
        x: 388,
        y: 817,
    },
    {
        common: {
            hash: '173eff56dc5df3d3',
            den: 11,
        },
        rare: {
            hash: 'ba805a67100fd091',
            den: 49,
        },
        location: 7,
        x: 443,
        y: 830,
    },
    {
        common: {
            hash: '173f0356dc5dfa9f',
            den: 15,
        },
        rare: {
            hash: '45009ed99cf56e51',
            den: 83,
        },
        location: 7,
        x: 410,
        y: 952,
    },
    {
        common: {
            hash: '173eff56dc5df3d3',
            den: 11,
        },
        rare: {
            hash: '450097d99cf5626c',
            den: 84,
        },
        location: 7,
        x: 447,
        y: 815,
    },
    {
        common: {
            hash: '173efe56dc5df220',
            den: 10,
        },
        rare: {
            hash: 'ba805967100fcede',
            den: 48,
        },
        location: 11,
        x: 393,
        y: 781,
    },
    {
        common: {
            hash: '17501b56dc6c969a',
            den: 41,
        },
        rare: {
            hash: 'ba8a496710180833',
            den: 76,
        },
        location: 11,
        x: 314,
        y: 755,
    },
    {
        common: {
            hash: '17490756dc6655f0',
            den: 29,
        },
        rare: {
            hash: 'ba83d9671012de35',
            den: 60,
        },
        location: 11,
        x: 440,
        y: 658,
    },
    {
        common: {
            hash: '17501b56dc6c969a',
            den: 41,
        },
        rare: {
            hash: 'ba8a496710180833',
            den: 76,
        },
        location: 11,
        x: 281,
        y: 717,
    },
    {
        common: {
            hash: '17490756dc6655f0',
            den: 29,
        },
        rare: {
            hash: 'ba83d9671012de35',
            den: 60,
        },
        location: 11,
        x: 440,
        y: 703,
    },
    {
        common: {
            hash: '17490756dc6655f0',
            den: 29,
        },
        rare: {
            hash: '450425d99cf8898d',
            den: 86,
        },
        location: 4,
        x: 310,
        y: 654,
    },
    {
        common: {
            hash: '173efe56dc5df220',
            den: 10,
        },
        rare: {
            hash: 'ba805967100fcede',
            den: 48,
        },
        location: 11,
        x: 443,
        y: 792,
    },
    {
        common: {
            hash: '173f0256dc5df8ec',
            den: 14,
        },
        rare: {
            hash: 'ba805367100fc4ac',
            den: 50,
        },
        location: 10,
        x: 412,
        y: 533,
    },
    {
        common: {
            hash: '17458a56dc634bb2',
            den: 30,
        },
        rare: {
            hash: 'ba83d9671012de35',
            den: 60,
        },
        location: 10,
        x: 345,
        y: 537,
    },
    {
        common: {
            hash: '17491456dc666c07',
            den: 24,
        },
        rare: {
            hash: 'ba83dd671012e501',
            den: 56,
        },
        location: 10,
        x: 365,
        y: 559,
    },
    {
        common: {
            hash: '17501c56dc6c984d',
            den: 40,
        },
        rare: {
            hash: 'ba8746671015cd43',
            den: 65,
        },
        location: 10,
        x: 408,
        y: 570,
    },
    {
        common: {
            hash: '17458656dc6344e6',
            den: 34,
        },
        rare: {
            hash: '45009dd99cf56c9e',
            den: 82,
        },
        location: 1,
        x: 193,
        y: 295,
    },
    {
        common: {
            hash: '173f0156dc5df739',
            den: 13,
        },
        rare: {
            hash: '450424d99cf887da',
            den: 87,
        },
        location: 1,
        x: 274,
        y: 321,
    },
    {
        common: {
            hash: '17428c56dc611941',
            den: 8,
        },
        rare: {
            hash: 'ba805d67100fd5aa',
            den: 44,
        },
        location: 1,
        x: 328,
        y: 330,
    },
    {
        common: {
            hash: '173f0456dc5dfc52',
            den: 16,
        },
        rare: {
            hash: 'ba83e1671012ebcd',
            den: 52,
        },
        location: 1,
        x: 370,
        y: 452,
    },
    {
        common: {
            hash: '17501c56dc6c984d',
            den: 40,
        },
        rare: {
            hash: 'ba8746671015cd43',
            den: 65,
        },
        location: 1,
        x: 224,
        y: 282,
    },
    {
        common: {
            hash: '17428856dc611275',
            den: 4,
        },
        rare: {
            hash: '45041fd99cf87f5b',
            den: 92,
        },
        location: 1,
        x: 342,
        y: 312,
    },
    {
        common: {
            hash: '17428756dc6110c2',
            den: 5,
        },
        rare: {
            hash: 'ba805767100fcb78',
            den: 46,
        },
        location: 1,
        x: 340,
        y: 269,
    },
    {
        common: {
            hash: '17458456dc634180',
            den: 36,
        },
        rare: {
            hash: 'ba8748671015d0a9',
            den: 63,
        },
        location: 1,
        x: 305,
        y: 323,
    },
    {
        common: {
            hash: '17491556dc666dba',
            den: 27,
        },
        rare: {
            hash: 'ba83da671012dfe8',
            den: 59,
        },
        location: 1,
        x: 334,
        y: 368,
    },
    {
        common: {
            hash: '17501c56dc6c984d',
            den: 40,
        },
        rare: {
            hash: 'ba874b671015d5c2',
            den: 66,
        },
        location: 14,
        x: 343,
        y: 222,
    },
    {
        common: {
            hash: '17428356dc6109f6',
            den: 1,
        },
        rare: {
            hash: '45009cd99cf56aeb',
            den: 81,
        },
        location: 14,
        x: 348,
        y: 195,
    },
    {
        common: {
            hash: '17428156dc610690',
            den: 3,
        },
        rare: {
            hash: 'ba805467100fc65f',
            den: 51,
        },
        location: 14,
        x: 200,
        y: 244,
    },
    {
        common: {
            hash: '173f0756dc5e016b',
            den: 19,
        },
        rare: {
            hash: '45009bd99cf56938',
            den: 80,
        },
        location: 14,
        x: 305,
        y: 183,
    },
    {
        common: {
            hash: '17428656dc610f0f',
            den: 6,
        },
        rare: {
            hash: 'ba805c67100fd3f7',
            den: 43,
        },
        location: 14,
        x: 348,
        y: 180,
    },
    {
        common: {
            hash: '17491556dc666dba',
            den: 27,
        },
        rare: {
            hash: 'ba83da671012dfe8',
            den: 59,
        },
        location: 14,
        x: 260,
        y: 199,
    },
    {
        common: {
            hash: '17428256dc610843',
            den: 2,
        },
        rare: {
            hash: 'ba805367100fc4ac',
            den: 50,
        },
        location: 14,
        x: 211,
        y: 205,
    },
    {
        common: {
            hash: '173f0056dc5df586',
            den: 12,
        },
        rare: {
            hash: '450098d99cf5641f',
            den: 85,
        },
        location: 14,
        x: 303,
        y: 242,
    },
    {
        common: {
            hash: '17491256dc6668a1',
            den: 22,
        },
        rare: {
            hash: 'ba83de671012e6b4',
            den: 55,
        },
        location: 14,
        x: 324,
        y: 209,
    },
    {
        common: {
            hash: '173f0256dc5df8ec',
            den: 14,
        },
        rare: {
            hash: 'ba805367100fc4ac',
            den: 50,
        },
        location: 14,
        x: 326,
        y: 219,
    },
    {
        common: {
            hash: '173f0656dc5dffb8',
            den: 18,
        },
        rare: {
            hash: 'ba83df671012e867',
            den: 54,
        },
        location: 14,
        x: 330,
        y: 215,
    },
    {
        common: {
            hash: '17458756dc634699',
            den: 35,
        },
        rare: {
            hash: 'ba8748671015d0a9',
            den: 63,
        },
        location: 14,
        x: 282,
        y: 195,
    },
    {
        common: {
            hash: '173f0556dc5dfe05',
            den: 17,
        },
        rare: {
            hash: '45041ed99cf87da8',
            den: 93,
        },
        location: 3,
        x: 265,
        y: 139,
    },
    {
        common: {
            hash: '173f0556dc5dfe05',
            den: 17,
        },
        rare: {
            hash: 'ba83e1671012ebcd',
            den: 52,
        },
        location: 3,
        x: 307,
        y: 93,
    },
    {
        common: {
            hash: '173f0356dc5dfa9f',
            den: 15,
        },
        rare: {
            hash: '45041ed99cf87da8',
            den: 93,
        },
        location: 3,
        x: 331,
        y: 84,
    },
    {
        common: {
            hash: '17428b56dc61178e',
            den: 9,
        },
        rare: {
            hash: 'ba8a4e67101810b2',
            den: 75,
        },
        location: 3,
        x: 219,
        y: 99,
    },
    {
        common: {
            hash: '173eff56dc5df3d3',
            den: 11,
        },
        rare: {
            hash: 'ba805a67100fd091',
            den: 49,
        },
        location: 3,
        x: 243,
        y: 120,
    },
    {
        common: {
            hash: '173efe56dc5df220',
            den: 10,
        },
        rare: {
            hash: 'ba805967100fcede',
            den: 48,
        },
        location: 3,
        x: 262,
        y: 174,
    },
    {
        common: {
            hash: '17490f56dc666388',
            den: 21,
        },
        rare: {
            hash: 'ba83de671012e6b4',
            den: 55,
        },
        location: 3,
        x: 283,
        y: 98,
    },
    {
        common: {
            hash: '17491056dc66653b',
            den: 20,
        },
        rare: {
            hash: 'ba83df671012e867',
            den: 54,
        },
        location: 3,
        x: 304,
        y: 112,
    },
    {
        common: {
            hash: '17490856dc6657a3',
            den: 28,
        },
        rare: {
            hash: 'ba805c67100fd3f7',
            den: 43,
        },
        location: 6,
        x: 306,
        y: 145,
    },
    {
        common: {
            hash: '17458456dc634180',
            den: 36,
        },
        rare: {
            hash: '450423d99cf88627',
            den: 88,
        },
        location: 3,
        x: 214,
        y: 168,
    },
    {
        common: {
            hash: '17501a56dc6c94e7',
            den: 42,
        },
        rare: {
            hash: 'ba874c671015d775',
            den: 67,
        },
        location: 6,
        x: 334,
        y: 145,
    },
    {
        common: {
            hash: '17458456dc634180',
            den: 36,
        },
        rare: {
            hash: 'ba874a671015d40f',
            den: 69,
        },
        location: 6,
        x: 347,
        y: 103,
    },
    {
        common: {
            hash: '17491556dc666dba',
            den: 27,
        },
        rare: {
            hash: 'ba874f671015dc8e',
            den: 70,
        },
        location: 6,
        x: 363,
        y: 88,
    },
    {
        common: {
            hash: '17491356dc666a54',
            den: 25,
        },
        rare: {
            hash: 'ba8a4d6710180eff',
            den: 72,
        },
        location: 6,
        x: 338,
        y: 122,
    },
    {
        common: {
            hash: '173f0056dc5df586',
            den: 12,
        },
        rare: {
            hash: 'ba805e67100fd75d',
            den: 45,
        },
        location: 8,
        x: 339,
        y: 35,
    },
    {
        common: {
            hash: '17458856dc63484c',
            den: 32,
        },
        rare: {
            hash: 'ba83d8671012dc82',
            den: 61,
        },
        location: 8,
        x: 310,
        y: 65,
    },
    {
        common: {
            hash: '17458a56dc634bb2',
            den: 30,
        },
        rare: {
            hash: 'ba83dc671012e34e',
            den: 57,
        },
        location: 8,
        x: 202,
        y: 34,
    },
    {
        common: {
            hash: '173f0756dc5e016b',
            den: 19,
        },
        rare: {
            hash: 'ba83e0671012ea1a',
            den: 53,
        },
        location: 8,
        x: 237,
        y: 67,
    },
    {
        common: {
            hash: '17491156dc6666ee',
            den: 23,
        },
        rare: {
            hash: 'ba8a4c6710180d4c',
            den: 73,
        },
        location: 8,
        x: 183,
        y: 47,
    },
    {
        common: {
            hash: '17458956dc6349ff',
            den: 33,
        },
        rare: {
            hash: 'ba8747671015cef6',
            den: 62,
        },
        location: 8,
        x: 221,
        y: 50,
    },
    {
        common: {
            hash: '173f0256dc5df8ec',
            den: 14,
        },
        rare: {
            hash: 'ba8749671015d25c',
            den: 68,
        },
        location: 8,
        x: 354,
        y: 60,
    },
    {
        common: {
            hash: '17458a56dc634bb2',
            den: 30,
        },
        rare: {
            hash: 'ba83d9671012de35',
            den: 60,
        },
        location: 5,
        x: 181,
        y: 185,
    },
    {
        common: {
            hash: '17491156dc6666ee',
            den: 23,
        },
        rare: {
            hash: 'ba83de671012e6b4',
            den: 55,
        },
        location: 5,
        x: 199,
        y: 145,
    },
    {
        common: {
            hash: '173f0656dc5dffb8',
            den: 18,
        },
        rare: {
            hash: 'ba8750671015de41',
            den: 71,
        },
        location: 5,
        x: 193,
        y: 173,
    },
    {
        common: {
            hash: '17458856dc63484c',
            den: 32,
        },
        rare: {
            hash: '450422d99cf88474',
            den: 89,
        },
        location: 5,
        x: 202,
        y: 95,
    },
    {
        common: {
            hash: '17491456dc666c07',
            den: 24,
        },
        rare: {
            hash: '4500a1d99cf5736a',
            den: 78,
        },
        location: 5,
        x: 185,
        y: 135,
    },
    {
        common: {
            hash: '17491356dc666a54',
            den: 25,
        },
        rare: {
            hash: 'ba83dd671012e501',
            den: 56,
        },
        location: 9,
        x: 170,
        y: 35,
    },
    {
        common: {
            hash: '173f0656dc5dffb8',
            den: 18,
        },
        rare: {
            hash: '4500a0d99cf571b7',
            den: 77,
        },
        location: 9,
        x: 128,
        y: 58,
    },
    {
        common: {
            hash: '17428c56dc611941',
            den: 8,
        },
        rare: {
            hash: 'ba805d67100fd5aa',
            den: 44,
        },
        location: 9,
        x: 161,
        y: 80,
    },
    {
        common: {
            hash: '17458656dc6344e6',
            den: 34,
        },
        rare: {
            hash: 'ba8a4f6710181265',
            den: 74,
        },
        location: 9,
        x: 143,
        y: 39,
    },
    {
        common: {
            hash: '79b25a4f80255a38',
            den: 115,
        },
        rare: {
            hash: 'c8ea8c1618ab0a58',
            den: 116,
        },
        location: 17,
        x: 643,
        y: 822,
    },
    {
        common: {
            hash: 'e2c6e5e725342f4a',
            den: 125,
        },
        rare: {
            hash: '89955cc3a594e51a',
            den: 126,
        },
        location: 17,
        x: 770,
        y: 794,
    },
    {
        common: {
            hash: '6d015b7858eb5119',
            den: 109,
        },
        rare: {
            hash: '53441b80e563ef1f',
            den: 110,
        },
        location: 17,
        x: 723,
        y: 812,
    },
    {
        common: {
            hash: '4257e50e1c471596',
            den: 133,
        },
        rare: {
            hash: 'fe9697f9799c65be',
            den: 134,
        },
        location: 17,
        x: 862,
        y: 770,
    },
    {
        common: {
            hash: '2998f2424d0353eb',
            den: 111,
        },
        rare: {
            hash: 'ae57b2a84974c3a1',
            den: 112,
        },
        location: 17,
        x: 673,
        y: 766,
    },
    {
        common: {
            hash: '5b72bfac0ff3f885',
            den: 113,
        },
        rare: {
            hash: '316e6b5e74bc7aa3',
            den: 114,
        },
        location: 17,
        x: 882,
        y: 745,
    },
    {
        common: {
            hash: '21f6c965b3513d5e',
            den: 99,
        },
        rare: {
            hash: 'd8f100cde5822516',
            den: 100,
        },
        location: 17,
        x: 661,
        y: 838,
    },
    {
        common: {
            hash: '6e6b46639f77f0c8',
            den: 105,
        },
        rare: {
            hash: '1c1962488c012ee8',
            den: 106,
        },
        location: 17,
        x: 683,
        y: 792,
    },
    {
        common: {
            hash: 'bc3d01fff751cde4',
            den: 123,
        },
        rare: {
            hash: '6f948f09933cdfc',
            den: 124,
        },
        location: 17,
        x: 831,
        y: 770,
    },
    {
        common: {
            hash: '4257e30e1c471230',
            den: 137,
        },
        rare: {
            hash: 'fe9695f9799c6258',
            den: 138,
        },
        location: 17,
        x: 727,
        y: 779,
    },
    {
        common: {
            hash: '4257e40e1c4713e3',
            den: 135,
        },
        rare: {
            hash: 'fe9696f9799c640b',
            den: 136,
        },
        location: 18,
        x: 662,
        y: 681,
    },
    {
        common: {
            hash: '2998f2424d0353eb',
            den: 111,
        },
        rare: {
            hash: 'ae57b2a84974c3a1',
            den: 112,
        },
        location: 18,
        x: 741,
        y: 680,
    },
    {
        common: {
            hash: 'e2c6e5e725342f4a',
            den: 125,
        },
        rare: {
            hash: '89955cc3a594e51a',
            den: 126,
        },
        location: 18,
        x: 697,
        y: 645,
    },
    {
        common: {
            hash: 'c63dec8a65b5c540',
            den: 121,
        },
        rare: {
            hash: '6aebee2a2d6d8470',
            den: 122,
        },
        location: 18,
        x: 732,
        y: 631,
    },
    {
        common: {
            hash: '5c9a35ca819b38c8',
            den: 119,
        },
        rare: {
            hash: 'f9222e1acdf486e8',
            den: 120,
        },
        location: 18,
        x: 634,
        y: 623,
    },
    {
        common: {
            hash: 'b8a5e528bfee71bc',
            den: 117,
        },
        rare: {
            hash: 'df017f3fefba2704',
            den: 118,
        },
        location: 18,
        x: 603,
        y: 591,
    },
    {
        common: {
            hash: '21f6c865b3513bab',
            den: 101,
        },
        rare: {
            hash: 'd8f0ffcde5822363',
            den: 102,
        },
        location: 18,
        x: 667,
        y: 614,
    },
    {
        common: {
            hash: '21f6c965b3513d5e',
            den: 99,
        },
        rare: {
            hash: 'd8f100cde5822516',
            den: 100,
        },
        location: 18,
        x: 609,
        y: 668,
    },
    {
        common: {
            hash: '4257e50e1c471596',
            den: 133,
        },
        rare: {
            hash: 'fe9697f9799c65be',
            den: 134,
        },
        location: 18,
        x: 554,
        y: 577,
    },
    {
        common: {
            hash: '6d015b7858eb5119',
            den: 109,
        },
        rare: {
            hash: '53441b80e563ef1f',
            den: 110,
        },
        location: 19,
        x: 533,
        y: 524,
    },
    {
        common: {
            hash: 'db8629cba3383296',
            den: 154,
        },
        rare: {
            hash: '4f1e561dd73ed3d8',
            den: 145,
        },
        location: 19,
        x: 687,
        y: 535,
    },
    {
        common: {
            hash: '6e6b46639f77f0c8',
            den: 105,
        },
        rare: {
            hash: '1c1962488c012ee8',
            den: 106,
        },
        location: 19,
        x: 622,
        y: 521,
    },
    {
        common: {
            hash: 'e2c6e5e725342f4a',
            den: 125,
        },
        rare: {
            hash: '89955cc3a594e51a',
            den: 126,
        },
        location: 19,
        x: 578,
        y: 512,
    },
    {
        common: {
            hash: '5c9a35ca819b38c8',
            den: 119,
        },
        rare: {
            hash: 'f9222e1acdf486e8',
            den: 120,
        },
        location: 19,
        x: 636,
        y: 492,
    },
    {
        common: {
            hash: '4257e40e1c4713e3',
            den: 135,
        },
        rare: {
            hash: 'fe9696f9799c640b',
            den: 136,
        },
        location: 19,
        x: 553,
        y: 529,
    },
    {
        common: {
            hash: '5b72bfac0ff3f885',
            den: 113,
        },
        rare: {
            hash: '316e6b5e74bc7aa3',
            den: 114,
        },
        location: 20,
        x: 488,
        y: 480,
    },
    {
        common: {
            hash: '4257e40e1c4713e3',
            den: 135,
        },
        rare: {
            hash: 'fe9696f9799c640b',
            den: 136,
        },
        location: 20,
        x: 483,
        y: 556,
    },
    {
        common: {
            hash: '6e6b46639f77f0c8',
            den: 105,
        },
        rare: {
            hash: '1c1962488c012ee8',
            den: 106,
        },
        location: 20,
        x: 465,
        y: 605,
    },
    {
        common: {
            hash: 'bc3d01fff751cde4',
            den: 123,
        },
        rare: {
            hash: '6f948f09933cdfc',
            den: 124,
        },
        location: 20,
        x: 446,
        y: 649,
    },
    {
        common: {
            hash: '60ef1d711ae30cf0',
            den: 117,
        },
        rare: {
            hash: 'c80756327d5de060',
            den: 118,
        },
        location: 20,
        x: 453,
        y: 561,
    },
    {
        common: {
            hash: '4257e30e1c471230',
            den: 137,
        },
        rare: {
            hash: 'fe9695f9799c6258',
            den: 138,
        },
        location: 20,
        x: 320,
        y: 526,
    },
    {
        common: {
            hash: 'b8a5e528bfee71bc',
            den: 103,
        },
        rare: {
            hash: 'df017f3fefba2704',
            den: 104,
        },
        location: 20,
        x: 442,
        y: 609,
    },
    {
        common: {
            hash: '4c12cee7784c8b8',
            den: 127,
        },
        rare: {
            hash: '7288f0346fd3cdd8',
            den: 128,
        },
        location: 20,
        x: 412,
        y: 566,
    },
    {
        common: {
            hash: '50eaf4685fa07085',
            den: 129,
        },
        rare: {
            hash: 'f9280759d6cc62a3',
            den: 130,
        },
        location: 21,
        x: 947,
        y: 506,
    },
    {
        common: {
            hash: 'bc3d01fff751cde4',
            den: 123,
        },
        rare: {
            hash: '6f948f09933cdfc',
            den: 124,
        },
        location: 22,
        x: 912,
        y: 467,
    },
    {
        common: {
            hash: '5584521f1e549486',
            den: 156,
        },
        rare: {
            hash: '55846e1f1e54c41a',
            den: 157,
        },
        location: 22,
        x: 925,
        y: 433,
    },
    {
        common: {
            hash: 'a178d4769765abac',
            den: 107,
        },
        rare: {
            hash: 'f4a830850f51d034',
            den: 108,
        },
        location: 22,
        x: 913,
        y: 408,
    },
    {
        common: {
            hash: 'c63dec8a65b5c540',
            den: 121,
        },
        rare: {
            hash: '6aebee2a2d6d8470',
            den: 122,
        },
        location: 22,
        x: 895,
        y: 365,
    },
    {
        common: {
            hash: '60ef1d711ae30cf0',
            den: 103,
        },
        rare: {
            hash: 'c80756327d5de060',
            den: 104,
        },
        location: 23,
        x: 526,
        y: 650,
    },
    {
        common: {
            hash: '4257e40e1c4713e3',
            den: 135,
        },
        rare: {
            hash: '4f1e5b1dd73edc57',
            den: 148,
        },
        location: 23,
        x: 576,
        y: 714,
    },
    {
        common: {
            hash: '4c12cee7784c8b8',
            den: 127,
        },
        rare: {
            hash: '7288f0346fd3cdd8',
            den: 128,
        },
        location: 23,
        x: 565,
        y: 726,
    },
    {
        common: {
            hash: '50eaf4685fa07085',
            den: 129,
        },
        rare: {
            hash: 'f9280759d6cc62a3',
            den: 130,
        },
        location: 23,
        x: 586,
        y: 726,
    },
    {
        common: {
            hash: '4257e50e1c471596',
            den: 133,
        },
        rare: {
            hash: 'fe9697f9799c65be',
            den: 134,
        },
        location: 23,
        x: 621,
        y: 749,
    },
    {
        common: {
            hash: '21f6c865b3513bab',
            den: 101,
        },
        rare: {
            hash: 'd8f0ffcde5822363',
            den: 102,
        },
        location: 23,
        x: 528,
        y: 695,
    },
    {
        common: {
            hash: '5b72bfac0ff3f885',
            den: 113,
        },
        rare: {
            hash: '316e6b5e74bc7aa3',
            den: 114,
        },
        location: 24,
        x: 408,
        y: 809,
    },
    {
        common: {
            hash: 'b0c9af2202b0a19e',
            den: 131,
        },
        rare: {
            hash: '4f1e5c1dd73ede0a',
            den: 151,
        },
        location: 24,
        x: 426,
        y: 790,
    },
    {
        common: {
            hash: '4257e30e1c471230',
            den: 137,
        },
        rare: {
            hash: 'fe9695f9799c6258',
            den: 138,
        },
        location: 24,
        x: 360,
        y: 850,
    },
    {
        common: {
            hash: 'a178d4769765abac',
            den: 107,
        },
        rare: {
            hash: 'f4a830850f51d034',
            den: 108,
        },
        location: 24,
        x: 327,
        y: 787,
    },
    {
        common: {
            hash: '5c9a35ca819b38c8',
            den: 119,
        },
        rare: {
            hash: 'f9222e1acdf486e8',
            den: 120,
        },
        location: 25,
        x: 707,
        y: 421,
    },
    {
        common: {
            hash: 'db8629cba3383296',
            den: 154,
        },
        rare: {
            hash: '4f1e561dd73ed3d8',
            den: 145,
        },
        location: 25,
        x: 832,
        y: 398,
    },
    {
        common: {
            hash: 'b8a5e528bfee71bc',
            den: 117,
        },
        rare: {
            hash: 'df017f3fefba2704',
            den: 118,
        },
        location: 25,
        x: 591,
        y: 430,
    },
    {
        common: {
            hash: '79b25a4f80255a38',
            den: 115,
        },
        rare: {
            hash: 'c8ea8c1618ab0a58',
            den: 116,
        },
        location: 25,
        x: 666,
        y: 334,
    },
    {
        common: {
            hash: '2998f2424d0353eb',
            den: 111,
        },
        rare: {
            hash: 'ae57b2a84974c3a1',
            den: 112,
        },
        location: 25,
        x: 758,
        y: 338,
    },
    {
        common: {
            hash: '6d015b7858eb5119',
            den: 109,
        },
        rare: {
            hash: '4f1e5a1dd73edaa4',
            den: 149,
        },
        location: 25,
        x: 719,
        y: 377,
    },
    {
        common: {
            hash: '21f6c865b3513bab',
            den: 101,
        },
        rare: {
            hash: 'd8f0ffcde5822363',
            den: 102,
        },
        location: 25,
        x: 659,
        y: 397,
    },
    {
        common: {
            hash: '60ef1d711ae30cf0',
            den: 103,
        },
        rare: {
            hash: '4f1e5d1dd73edfbd',
            den: 150,
        },
        location: 26,
        x: 665,
        y: 243,
    },
    {
        common: {
            hash: '5b72bfac0ff3f885',
            den: 113,
        },
        rare: {
            hash: '316e6b5e74bc7aa3',
            den: 114,
        },
        location: 26,
        x: 784,
        y: 212,
    },
    {
        common: {
            hash: '79b25a4f80255a38',
            den: 115,
        },
        rare: {
            hash: 'c8ea8c1618ab0a58',
            den: 116,
        },
        location: 26,
        x: 881,
        y: 235,
    },
    {
        common: {
            hash: '6b37a94863bf68c0',
            den: 155,
        },
        rare: {
            hash: '4f1e591dd73ed8f1',
            den: 146,
        },
        location: 27,
        x: 321,
        y: 1004,
    },
    {
        common: {
            hash: '4257ea0e1c471e15',
            den: 139,
        },
        rare: {
            hash: 'fe969cf9799c6e3d',
            den: 140,
        },
        location: 27,
        x: 782,
        y: 962,
    },
    {
        common: {
            hash: '40bdbe4f3bcbac86',
            den: 152,
        },
        rare: {
            hash: '9fdf11a0cde96b2e',
            den: 153,
        },
        location: 27,
        x: 1040,
        y: 752,
    },
    {
        common: {
            hash: 'c63dec8a65b5c540',
            den: 121,
        },
        rare: {
            hash: '6aebee2a2d6d8470',
            den: 122,
        },
        location: 27,
        x: 970,
        y: 701,
    },
    {
        common: {
            hash: '6d015b7858eb5119',
            den: 109,
        },
        rare: {
            hash: '53441b80e563ef1f',
            den: 110,
        },
        location: 27,
        x: 759,
        y: 1015,
    },
    {
        common: {
            hash: 'a178d4769765abac',
            den: 107,
        },
        rare: {
            hash: 'f4a830850f51d034',
            den: 108,
        },
        location: 27,
        x: 558,
        y: 1082,
    },
    {
        common: {
            hash: 'b0c9af2202b0a19e',
            den: 131,
        },
        rare: {
            hash: '3d6f1fcb3898d356',
            den: 132,
        },
        location: 27,
        x: 523,
        y: 993,
    },
    {
        common: {
            hash: '60ef1d711ae30cf0',
            den: 103,
        },
        rare: {
            hash: 'c80756327d5de060',
            den: 104,
        },
        location: 28,
        x: 129,
        y: 797,
    },
    {
        common: {
            hash: 'b8a5e528bfee71bc',
            den: 117,
        },
        rare: {
            hash: 'df017f3fefba2704',
            den: 118,
        },
        location: 28,
        x: 75,
        y: 658,
    },
    {
        common: {
            hash: '6b37a94863bf68c0',
            den: 155,
        },
        rare: {
            hash: '4f1e591dd73ed8f1',
            den: 146,
        },
        location: 28,
        x: 120,
        y: 523,
    },
    {
        common: {
            hash: '5c9a35ca819b38c8',
            den: 119,
        },
        rare: {
            hash: 'f9222e1acdf486e8',
            den: 120,
        },
        location: 28,
        x: 120,
        y: 547,
    },
    {
        common: {
            hash: '50eaf4685fa07085',
            den: 129,
        },
        rare: {
            hash: 'f9280759d6cc62a3',
            den: 130,
        },
        location: 28,
        x: 287,
        y: 559,
    },
    {
        common: {
            hash: '21f6c965b3513d5e',
            den: 99,
        },
        rare: {
            hash: 'd8f100cde5822516',
            den: 100,
        },
        location: 28,
        x: 258,
        y: 654,
    },
    {
        common: {
            hash: '4c12cee7784c8b8',
            den: 127,
        },
        rare: {
            hash: '7288f0346fd3cdd8',
            den: 128,
        },
        location: 28,
        x: 174,
        y: 852,
    },
    {
        common: {
            hash: '4257ea0e1c471e15',
            den: 139,
        },
        rare: {
            hash: 'fe969cf9799c6e3d',
            den: 140,
        },
        location: 28,
        x: 162,
        y: 808,
    },
    {
        common: {
            hash: 'a178d4769765abac',
            den: 107,
        },
        rare: {
            hash: 'f4a830850f51d034',
            den: 108,
        },
        location: 28,
        x: 162,
        y: 763,
    },
    {
        common: {
            hash: 'e2c6e5e725342f4a',
            den: 125,
        },
        rare: {
            hash: '89955cc3a594e51a',
            den: 126,
        },
        location: 29,
        x: 299,
        y: 356,
    },
    {
        common: {
            hash: '21f6c965b3513d5e',
            den: 99,
        },
        rare: {
            hash: 'd8f100cde5822516',
            den: 100,
        },
        location: 29,
        x: 214,
        y: 349,
    },
    {
        common: {
            hash: '2998f2424d0353eb',
            den: 111,
        },
        rare: {
            hash: 'ae57b2a84974c3a1',
            den: 112,
        },
        location: 29,
        x: 185,
        y: 302,
    },
    {
        common: {
            hash: '4257ea0e1c471e15',
            den: 139,
        },
        rare: {
            hash: 'fe969cf9799c6e3d',
            den: 140,
        },
        location: 29,
        x: 247,
        y: 298,
    },
    {
        common: {
            hash: '4257e30e1c471230',
            den: 137,
        },
        rare: {
            hash: '4f1e581dd73ed73e',
            den: 147,
        },
        location: 29,
        x: 271,
        y: 273,
    },
    {
        common: {
            hash: 'b0c9af2202b0a19e',
            den: 131,
        },
        rare: {
            hash: '3d6f1fcb3898d356',
            den: 132,
        },
        location: 30,
        x: 468,
        y: 451,
    },
    {
        common: {
            hash: 'bc3d01fff751cde4',
            den: 123,
        },
        rare: {
            hash: '6f948f09933cdfc',
            den: 124,
        },
        location: 30,
        x: 605,
        y: 166,
    },
    {
        common: {
            hash: 'c63dec8a65b5c540',
            den: 121,
        },
        rare: {
            hash: '6aebee2a2d6d8470',
            den: 122,
        },
        location: 30,
        x: 672,
        y: 120,
    },
    {
        common: {
            hash: '4257ea0e1c471e15',
            den: 139,
        },
        rare: {
            hash: 'fe969cf9799c6e3d',
            den: 140,
        },
        location: 30,
        x: 716,
        y: 91,
    },
    {
        common: {
            hash: '6e6b46639f77f0c8',
            den: 105,
        },
        rare: {
            hash: '1c1962488c012ee8',
            den: 106,
        },
        location: 30,
        x: 597,
        y: 105,
    },
    {
        common: {
            hash: 'ea4c3915ea6f95a0',
            den: 143,
        },
        rare: {
            hash: '3ea9df3b7b2b5990',
            den: 144,
        },
        location: 31,
        x: 471,
        y: 152,
    },
    {
        common: {
            hash: 'ea4c3915ea6f95a0',
            den: 143,
        },
        rare: {
            hash: '3ea9df3b7b2b5990',
            den: 144,
        },
        location: 31,
        x: 490,
        y: 194,
    },
    {
        common: {
            hash: 'ea4c3915ea6f95a0',
            den: 143,
        },
        rare: {
            hash: '3ea9df3b7b2b5990',
            den: 144,
        },
        location: 31,
        x: 464,
        y: 237,
    },
    {
        common: {
            hash: 'ea4c3915ea6f95a0',
            den: 143,
        },
        rare: {
            hash: '3ea9df3b7b2b5990',
            den: 144,
        },
        location: 31,
        x: 413,
        y: 237,
    },
    {
        common: {
            hash: 'ea4c3915ea6f95a0',
            den: 143,
        },
        rare: {
            hash: '3ea9df3b7b2b5990',
            den: 144,
        },
        location: 31,
        x: 386,
        y: 195,
    },
    {
        common: {
            hash: 'ea4c3915ea6f95a0',
            den: 143,
        },
        rare: {
            hash: '3ea9df3b7b2b5990',
            den: 144,
        },
        location: 31,
        x: 414,
        y: 148,
    },
];

